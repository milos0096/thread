package gosti;

public class Musterija {
    private int vremeB;
    private static int idG = 0;
    private int id;
    private double sredstva;

    public Musterija(int vremeB, double sredstva) {
        this.vremeB = vremeB;
        this.id = ++idG;
        this.sredstva = sredstva;
    }

    public int getVremeB() {
        return vremeB;
    }

    public void setVremeB(int vremeB) {
        this.vremeB = vremeB;
    }

    public double getSredstva() {
        return sredstva;
    }

    public void smanjiDan() {
        vremeB--;
    }

    public String toString() {
        return "[Musterija " + id + " / b:" + vremeB + "]";
    }
}

